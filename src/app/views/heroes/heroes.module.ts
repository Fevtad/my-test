import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeroesRoutingModule } from './heroes-routing.module';
import { HeroesComponent } from './heroes.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { environment } from '../../../environments/environment';
import { HeroInMemoryDataService } from './hero-in-memory-data.service';
import { SearchHeroPipe } from './search-hero.pipe';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [HeroesComponent, SearchHeroPipe],
  imports: [
    CommonModule,
    FormsModule,
    // HttpClientModule,
    // !environment.production ? HttpClientInMemoryWebApiModule.forRoot(HeroInMemoryDataService) : [],
    HeroesRoutingModule
  ]
})
export class HeroesModule { }
