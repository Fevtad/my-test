import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { Hero } from './hero.object';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  SERVER_URL = 'api/heroes';


  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers
  };

  constructor(private httpClient: HttpClient) { }

  public getHeroes() {
    return this.httpClient.get(this.SERVER_URL);
  }

  public getHero(heroId) {
    return this.httpClient.get(this.SERVER_URL + '/' + heroId);
    //  return this.httpClient.get(`${this.SERVER_URL}/${HeroId}`);
  }
  public createHero(Hero: Hero) {
    return this.httpClient.post(`${this.SERVER_URL}`, Hero)
  }

  public deleteHero(HeroId) {
    return this.httpClient.delete(`${this.SERVER_URL}/${HeroId}`)
  }
  public updateHero(Hero: Hero) {
    return this.httpClient.put(`${this.SERVER_URL}/${Hero.id}`, Hero)
  }
}
