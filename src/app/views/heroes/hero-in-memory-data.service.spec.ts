import { TestBed } from '@angular/core/testing';

import { HeroInMemoryDataService } from './hero-in-memory-data.service';

describe('HeroInMemoryDataService', () => {
  let service: HeroInMemoryDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeroInMemoryDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
