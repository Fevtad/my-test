import { Pipe, PipeTransform } from '@angular/core';
import { Hero } from './hero.object';

@Pipe({
  name: 'searchHero'
})
export class SearchHeroPipe implements PipeTransform {

  transform(items: Hero[], nameSearch: string) {
    if (items && items.length) {
      return items.filter(item => {
        if (nameSearch && item.name.toLowerCase().indexOf(nameSearch.toLowerCase()) === -1) {
          return false;
        }
        return true;
      });
    } else {
      return items;
    }
  }

}
