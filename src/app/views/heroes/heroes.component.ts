import { Component, OnInit } from '@angular/core';
import { Hero } from './hero.object';
import { HeroService } from './hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  list;
  heroes;
  nameSearch;


  constructor(private heroS: HeroService) { }

  ngOnInit(): void {
    this.heroS.deleteHero(7).subscribe(
      data => {
        console.log(data);
      },
    );
    // this.heroS.createHero(this.hero).subscribe(
    //   data => {
    //     console.log(data);
    //   }
    // );
    this.heroS.getHeroes().subscribe(
      data => {
        this.heroes = data;
        console.log(data);
      }
    );
  }

}
