import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero.object';

@Injectable({
  providedIn: 'root'
})
export class HeroInMemoryDataService implements InMemoryDbService {

  constructor() {}
  createDb() {
    let heroes: Hero[] = [
      { id: 1, name: 'h1' },
      { id: 2, name: 'h2' },
      { id: 3, name: 'h3' },
      { id: 4, name: 'h4' }
    ];

    let cars = [
      { id: 1, name: 'c1' , hero: [heroes[0], heroes[1]] },
      { id: 2, name: 'c2', hero: heroes[1] },
      { id: 3, name: 'c3', hero: heroes[2] },
      { id: 4, name: 'c4', hero: heroes[3] }
    ];
    let users = [
      { id: 1, name: 'u1', password: 'secret1', token: '123' },
      { id: 2, name: 'u2', passowrd: 'secret2', token: '456' },
    ]

    return {cars, heroes, users};
  }
}
