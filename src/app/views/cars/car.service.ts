import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  SERVER_URL = 'api/cars';


  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers
  };

  constructor(private httpClient: HttpClient) { }

  public getCars() {
    return this.httpClient.get(this.SERVER_URL);
  }

  public getCar(CarId) {
    return this.httpClient.get(this.SERVER_URL + '/' + CarId);
    //  return this.httpClient.get(`${this.SERVER_URL}/${CarId}`);
  }
  public createCar(Car: { id: number, amount: number, clientId: number, userId: number, description: string }) {
    return this.httpClient.post(`${this.SERVER_URL}`, Car)
  }

  public deleteCar(CarId) {
    return this.httpClient.delete(`${this.SERVER_URL}/${CarId}`)
  }
  public updateCar(Car: { id: number, amount: number, clientId: number, userId: number, description: string }) {
    return this.httpClient.put(`${this.SERVER_URL}/${Car.id}`, Car)
  }
}
