import { Component, OnInit } from '@angular/core';
import { CarService } from './car.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  constructor(private carS: CarService) { }

  ngOnInit(): void {
    // this.heroS.getHero(1).subscribe(
      this.carS.getCars().subscribe(
      data => {
        console.log(data);
      }
    );
  }

}
