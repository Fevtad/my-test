import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  SERVER_URL = 'api/users';


  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers
  };

  constructor(private httpClient: HttpClient) { }

  public createUser(User: { id: number, amount: number, clientId: number, userId: number, description: string }) {
    return this.httpClient.post(`${this.SERVER_URL}`, User)
  }

  public deleteUser(UserId) {
    return this.httpClient.delete(`${this.SERVER_URL}/${UserId}`)
  }
  public updateUser(User: { id: number, amount: number, clientId: number, userId: number, description: string }) {
    return this.httpClient.put(`${this.SERVER_URL}/${User.id}`, User)
  }

  public login(user) {
    return localStorage.setItem('usern',user.name);
  }

}
